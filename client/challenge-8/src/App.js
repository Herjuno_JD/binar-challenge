import './App.css';
import { useState } from 'react';

let DataAwal = [{
      id: 1,
      username:'ricky',
      email:'ricky@email.com',
      experience:90,
      lvl:10
    },
    {
      id: 2,
      username:'ricky1',
      email:'ricky1@email.com',
      experience:91,
      lvl:11
    },
    {
      id: 3,
      username:'ricky2',
      email:'ricky2@email.com',
      experience:92,
      lvl:12
    },
    {
      id: 4,
      username:'ricky3',
      email:'ricky3@email.com',
      experience:93,
      lvl:13
    }]

function App() {

  //State awal untuk pengisian form yang digunakan dalam fungsi menambah player
  const [formPlayer, setFormPlayer] = useState([
    {
      username:'',
      email:'',
      experience:0,
      lvl:0
    }
  ])

  //Fungsi untuk menangkap data-data element di bagian penambahan player
  const changeHandler = (e) => {
    setFormPlayer({
      ...formPlayer,
      [e.target.name] : e.target.value
    })
  }

  //Fungsi untuk menangkap data-data element di bagian mengubah data player
  const editHandler = (e) => {
    setFormEditPlayer({
      ...formEditPlayer,
      [e.target.name] : e.target.value
    })
    console.log(formEditPlayer)
  }

  //State awal untuk melakukan pencarian player
  const [search, setSearch] = useState("")

  //Fungsi untuk menangkap data-data element di bagian pencarian player
  function searchHandler(e){
    setSearch(e.target.value)
  }

  //Fungsi untuk melakukan pencarian yang ditempelkan pada tombol search
  //Fungsi ini menggunakan DataAwal (yang berada di luar fungsi App) sebagai nilai untuk variabel playerTemp
  //playerTemp ini nantinya akan dioper ke metode filter untuk mengecek
  //apakah nilai yang dimasukkan di form search memiliki kecocokan dengan nilai pada DataAwal
  //Nantinya fungsi ini akan menampilkan player yang sesuai dengan nilai pada form search
  function searchClick(){
    let playerTemp = DataAwal
    let playerFinal = playerTemp.filter((el) => {
      if(el.username.search(search) >= 0
      || el.email.search(search) >= 0
      || el.experience.toString().search(search) >= 0
      || el.lvl.toString().search(search) >= 0){
        return el
      }
    })
    //Jika fungsi dijalankan ketika form search kosong, masukkan nilai dari DataAwal kembali ke player
    if(search == ""){
      setPlayer(DataAwal);
    } else {
    setPlayer(playerFinal);
    }
  }

  //State awal yang memiliki nilai database player seperti variabel DataAwal
  const [player, setPlayer] = useState([
    {
      id: 1,
      username:'ricky',
      email:'ricky@email.com',
      experience:90,
      lvl:10
    },
    {
      id: 2,
      username:'ricky1',
      email:'ricky1@email.com',
      experience:91,
      lvl:11
    },
    {
      id: 3,
      username:'ricky2',
      email:'ricky2@email.com',
      experience:92,
      lvl:12
    },
    {
      id: 4,
      username:'ricky3',
      email:'ricky3@email.com',
      experience:93,
      lvl:13
    }
  ])

  //Fungsi untuk menambahkan data player baru sesuai dengan form yang diisi
  //Kemudian setFormPlayer digunakan untuk mereset nilai di form menjadi seperti semula
  const addData = () => {
    setPlayer([...player, formPlayer])
    setFormPlayer({
      username: '',
      email: '',
      experience: '',
      lvl: ''
    })
  }

  //State awal untuk pengisian form yang digunakan dalam fungsi mengubah data player
  const [formEditPlayer, setFormEditPlayer] = useState({
    id : '',
    username : '',
    email : '',
    experience : '',
    lvl : ''
  })

  //Fungsi ini digunakan untuk melempar data yang ingin diubah ke form edit player
  const editData = (id) => {
    player.filter((el) => {
      if(el.id === id){
        setFormEditPlayer(el)
      }
    })
  }

  //Fungsi ini melakukan finalisasi dari pengubahan data player
  //Pertama-tama, fungsi ini akan mencocokkan id dari data yang dilempar oleh fungsi editData
  //Kemudian isi dari form edit player akan dipindahkan ke data player dengan id yang bersangkutan
  //Setelah itu setFormEditPlayer untuk melakukan finalisasi pemindahan data
  //setFormEditPlayer kedua digunakan untuk mengosongkan kembali form edit player
  const btnEditData = () => {
    let playerTemp = player;
    let hasil = player.findIndex((el) => el.id === formEditPlayer.id)
    playerTemp[hasil].username = formEditPlayer.username
    playerTemp[hasil].email = formEditPlayer.email
    playerTemp[hasil].experience = formEditPlayer.experience
    playerTemp[hasil].lvl = formEditPlayer.lvl
    setFormEditPlayer(playerTemp)
    setFormEditPlayer({
      id : '',
      username : '',
      email : '',
      experience : '',
      lvl : ''
    })
  }

  return (
    <div className="App">
      
    <div className="KotakSearch">
      <h2>Search Player</h2>
      <input
        type="text"
        value={search}
        onChange={searchHandler}
      />
      <button onClick={searchClick}>Search</button> 
    </div>

      <div className="KotakPlayer">
      {
        player.map((data, index) => {
          return(
            <div className="Datauser">
              <h5>Username: {data.username}</h5>
              <h5>Email: {data.email}</h5>
              <h5>Experience: {data.experience}</h5>
              <h5>Lvl: {data.lvl}</h5>
              <button className="ButtonApp" onClick={() => editData(data.id)}>Edit</button>
            </div>
          )
        })
      }
      </div>
      <hr />
      <form className="KotakForm">
        <h2>Add Player</h2>
        <label>Username : <input
        type="text"
        value={formPlayer.username}
        name="username"
        onChange={changeHandler}
        /></label>
        <label>Email : <input
        type="text"
        value={formPlayer.email}
        name="email"
        onChange={changeHandler}
        /></label>
        <label>Experience : <input
        type="number"
        value={formPlayer.experience}
        name="experience"
        onChange={changeHandler}
        /></label>
        <label>Level : <input
        type="number"
        value={formPlayer.lvl}
        name="lvl"
        onChange={changeHandler}
        /></label>
      </form>

      <button className="ButtonApp" onClick={addData}>Tambah</button>
      <hr />
      <form className="KotakForm">
        <h2>Edit Player</h2>
        <label>Username : <input
        type="text"
        value={formEditPlayer.username}
        name="username"
        onChange={editHandler}
        /></label>
        <label>Email : <input
        type="text"
        value={formEditPlayer.email}
        name="email"
        onChange={editHandler}
        /></label>
        <label>Experience : <input
        type="number"
        value={formEditPlayer.experience}
        name="experience"
        onChange={editHandler}
        /></label>
        <label>Level : <input
        type="number"
        value={formEditPlayer.lvl}
        name="lvl"
        onChange={editHandler}
        /></label>
      </form>
      <button className="ButtonApp" onClick={btnEditData}>Submit</button>
    </div>

  );
}

export default App;
